package golangfifo

import (
	"container/list"
	"fmt"
	"sync"
)

//构建一个FIFO结构
type Queue struct {
	sync.Mutex
	List *list.List
}

//实例化FIFO
func NewQueue() *Queue {
	list := list.New()
	q := &Queue{List: list}
	return q
}

//返回list的大小
func (q *Queue) Size() int {
	return q.List.Len()
}

//将元素插入到FIFO中
func (q *Queue) Enqueue(v interface{}) *list.Element {
	q.Mutex.Lock()
	e := q.List.PushFront(v)
	q.Mutex.Unlock()
	return e
}

//将FIFO中的第一个元素读取出来并清除在list中的对应元素
func (q *Queue) Dequeue() *list.Element {
	q.Mutex.Lock()
	e := q.List.Back()
	if e != nil {
		q.List.Remove(e)
		q.Mutex.Unlock()
		return e
	}
	q.Mutex.Unlock()
	return nil
}

//查询FIFO中的所有元素
func (q *Queue) Query() {
	q.Mutex.Lock()
	for e := q.List.Back(); e != nil; e = e.Prev() {
		fmt.Println(e.Value)
	}
	q.Mutex.Unlock()
}
