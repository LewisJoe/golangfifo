package golangfifo

import (
	"fmt"
	"testing"
)

func Test(t *testing.T) {
	a := []byte{1, 2, 3, 4, 5}
	b := []byte{5, 7, 8, 9}
	c := []byte{8, 4, 6, 54, 23}
	d := []byte{8, 4, 6, 54, 23}
	q := NewQueue()
	fmt.Println(q.Enqueue(a).Value)
	fmt.Println(q.Enqueue(b).Value)
	fmt.Println(q.Enqueue(c).Value)
	fmt.Println(q.Enqueue(d).Value)
	fmt.Println("The list len is", q.Size())
	if e := q.Dequeue(); e != nil {
		fmt.Println("The element value is", e.Value)
	} else {
		fmt.Println("The list is empty...")
	}
	q.Dequeue()
	fmt.Println("When Dequeue the list size is", q.Size())
	q.Query()

}
